import time

from selenium.webdriver import Keys, ActionChains
from selenium.webdriver.common.by import By


def test_add_to_cart(main_page):
    requested_page = main_page.search_request("mirrors")
    time.sleep(3)
    product_page = requested_page.choose_element_position()
    time.sleep(5)
    product_page.switch_to_home_delivery()
    time.sleep(3)
    product_page.add_too_cart()
    time.sleep(3)
    cart = product_page.go_to_cart()

    assert cart.product_in_cart().is_displayed()


































