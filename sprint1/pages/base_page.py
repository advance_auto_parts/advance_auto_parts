
from selenium.webdriver import Chrome, ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.remote.webelement import WebElement
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

from sprint1.pages.cart import Cart


class BasePage:
    def __init__(self, driver: Chrome):
        self.driver = driver
        self.wait = WebDriverWait(self.driver, 20)
        self.action_chain = ActionChains(driver)

    def wait_for_element(self, locator):
        element: WebElement = self.wait.until(EC.presence_of_element_located(locator))
        return element

    def click(self, locator):
        element: WebElement = self.wait_for_element(locator)
        element.click()

    def go_to_cart(self):
        self.driver.find_element(By.XPATH, "//button[@aria-label='cartButton']").click()
        return Cart(self.driver)





