from selenium.common import NoSuchElementException
from selenium.webdriver.common.by import By
from selenium.webdriver.remote.webelement import WebElement

from sprint1.pages.base_page import BasePage


class Product(BasePage):
    def __init__(self, driver):
        super().__init__(driver)

    def switch_to_home_delivery(self):
        element: WebElement = self.driver.find_element(By.XPATH, "//div[@class='css-137n2pl']")
        element.click()

    def add_too_cart(self):
        element: WebElement = self.driver.find_element(By.XPATH, "//button[@class='primaryRedesign css-1eqcz9u']")
        element.click()

