import time

from selenium.common import NoSuchElementException
from selenium.webdriver.common.by import By
from selenium.webdriver import Chrome, ActionChains, Keys

from sprint1.pages.requested_page import RequestedPage
from sprint1.pages.base_page import BasePage


class HomePage(BasePage):
    def __init__(self, driver: Chrome):
        super().__init__(driver)

    def search_request(self, request: str):
        search_line = self.driver.find_element(By.XPATH, "//div[@class='search-input']/input")
        search_line.click()
        search_line.send_keys(request)
        search_line.send_keys(Keys.RETURN)
        return RequestedPage(self.driver)


