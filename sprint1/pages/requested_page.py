from selenium.common import NoSuchElementException
from selenium.webdriver.common.by import By
from selenium.webdriver.remote.webelement import WebElement

from sprint1.pages.product import Product
from sprint1.pages.base_page import BasePage


class RequestedPage(BasePage):
    def __init__(self, driver):
        super().__init__(driver)

    def choose_element_position(self):
        element: WebElement = self.driver.find_element(By.XPATH, "//p[@title='K-Source Mirror Assembly: Driver Side, Power Adjustment, Foldable, 1 Pk']")
        element.click()
        return Product(self.driver)


















