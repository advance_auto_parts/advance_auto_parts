import pytest as pytest
from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager
from sprint1.pages.home_page import HomePage
from selenium.webdriver.chrome.service import Service

@pytest.fixture(scope="session")
def driver():
    # driver = webdriver.Chrome()
    # driver.maximize_window()

    serv_obj = Service("C:/Users/leopa/PycharmProjects/AdvancedAutoParts/driver/chromedriver.exe")
    driver = webdriver.Chrome(service=serv_obj)
    driver.maximize_window()

    driver.get("https://shop.advanceautoparts.com/")

    yield driver

    driver.quit()


@pytest.fixture(scope="session")
def main_page(driver):
    yield HomePage(driver)
